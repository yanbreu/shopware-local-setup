# Shopware Local Env

## Requirements:
 - [Docker](https://docs.docker.com/install/#supported-platforms)
 - [Docker Compose](https://docs.docker.com/compose/install/)
 
## Setup

1. Folder Structure
    ```
    Project (root)
    ├── docker ─> this repo
    └── public ─> Shopware
    ```

2. Init docker config
    ```bash
    cd docker
    bash ./init-docker-env.sh
    ```
    
3. Adjust `.env` configuration

4. If needed uncomment `redis` or `elasticsearch` in `docker-compose.yml`
(Don't forget to uncomment the lines at `shopware-php` > `links`)

5. Build docker container
    ```bash
    docker-compose build
    ```

6. Start docker container
    ```bash
    docker-compose up --force-recreate -d
    ```
    
## Information

The Setup contains:

- Nginx <small>v1.15</small>
    - http2
    - https
- PHP-FPM <small>v7.1</small>
    - ioncube
- MariaDB <small>v10.3</small>
- Mailcatcher
    - UI on Port `1080`
- Redis <small>v5</small> *(if activated)*
- Elasticsearch <small>v6.3.2</small> *(if activated)*

All ports are forwarded to the host. If you want to access the db, just use the docker hosts IP and the default port 3306.