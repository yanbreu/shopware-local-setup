<VirtualHost *:80>
    ServerName ${HOST}

    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/html

    SetEnv SHOPWARE_ENV ${SHOPWARE_ENV}

    #LogLevel info ssl:warn

    ErrorLog ${APACHE_LOG_DIR}/${HOST}-error.log
    CustomLog ${APACHE_LOG_DIR}/${HOST}-access.log combined

    <Directory /var/www/html>
        Options -Indexes
        AllowOverride All
    </Directory>

    RewriteEngine on
    RewriteCond %{SERVER_NAME} =${HOST}
    RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
</VirtualHost>

<IfModule mod_ssl.c>
    <VirtualHost *:443>
        ServerName ${HOST}

        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html

        SetEnv SHOPWARE_ENV ${SHOPWARE_ENV}

        #LogLevel info ssl:warn

        ErrorLog ${APACHE_LOG_DIR}/${HOST}-error.log
        CustomLog ${APACHE_LOG_DIR}/${HOST}.log combined

        <Directory /var/www/html>
            Options -Indexes
            AllowOverride All
        </Directory>

        SSLCertificateFile /data/ssl/${HOST}.crt
        SSLCertificateKeyFile /data/ssl/${HOST}.key
        SSLEngine on

        # Intermediate configuration, tweak to your needs
        SSLProtocol             all -SSLv2 -SSLv3
        SSLCipherSuite          ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!3DES:!MD5:!PSK
        SSLHonorCipherOrder     on
        SSLCompression          off

        SSLOptions +StrictRequire

        # Add vhost name to log entries:
        LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\"" vhost_combined
        LogFormat "%v %h %l %u %t \"%r\" %>s %b" vhost_common
    </VirtualHost>
</IfModule>