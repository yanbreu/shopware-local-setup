#!/bin/bash

mkdir -p /etc/nginx/sites-enabled/;
export IFS=",";
for HOST in $HOSTNAMES;
do export HOST=$HOST;
    echo "Create vhost for ${HOST} with SHOPWARE_ENV = ${SHOPWARE_ENV}";
    envsubst '${HOST} ${SHOPWARE_ENV}' < /tmp/default.tpl > /etc/nginx/sites-enabled/$HOST.conf;
done

nginx -g "daemon off;"