#!/usr/bin/env bash

GID=$(id -g)
if [[ ! -f .env ]]; then
    cp .env.example .env
fi;

# Set line-ending to LF (Unix)
find . -name "*.sh" -exec sed -i 's/\r$//' {} \;

sed -i "s/^UID.*/UID\=${UID}/g" .env
sed -i "s/^GID.*/GID\=${GID}/g" .env
