server {
    listen 80;

    server_name ${HOST};
    return 301 https://$server_name$request_uri;
}


server {
    gzip on;
    listen 443 ssl http2;
    root /var/www/html;
    server_name ${HOST};

    ## Access and error logs.
    access_log /data/logs/${HOST}-access.log;
    error_log  /data/logs/${HOST}-error.log;

    set $shopware_env '${SHOPWARE_ENV}';

    ssl_certificate /data/ssl/${HOST}.crt; # cert.pem
    ssl_certificate_key /data/ssl/${HOST}.key; # key.key
    ssl_session_cache shared:SSL:10m;
    ssl_session_timeout  10m;

    client_max_body_size 24M;
    include global/shopware.conf;
}
